import math

def sincos(num):
    return (math.sin(num), math.cos(num))

def zad3():
    for x in range(91):
        print 'kat: %i sin: %f cos: %f' % (x, sincos(x)[0], sincos(x)[1])

zad3()
