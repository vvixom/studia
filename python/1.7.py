import sys

sam =  ['a', 'e', 'o', 'u', 'y', 'i']
sys_func = dir(sys)

dicto = {x: len(x) for x in sys_func if x[0] in sam}

for k, v in sorted(dicto.items()):
    print '%s: %i' % (k, v)
