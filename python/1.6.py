from random import randint

lista = [randint(0,9) for x in range(40)]

del lista[::2]

for i, x in enumerate(lista):
    if not i % 4:
        lista.insert(i, -1)

lista.append(-1)

print(lista)

for i, x in enumerate(lista):
    if not i % 4:
        print lista[i:i+4]
