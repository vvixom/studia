class Ulamek:
    def __init__(self, licznik, mianownik):
        self.licznik = licznik
        self.mianownik = mianownik

    def mnoz(self, ulamek):
        if not isinstance(ulamek, Ulamek):
            return 'Not valid instance'

        licz = ulamek.licznik * self.licznik
        mian = ulamek.mianownik * self.mianownik

        return Ulamek(licz, mian)

    def podzielony(self):
        return self.licznik/self.mianownik

    def humanize(self):
        return "[%i/%i]" % (self.licznik, self.mianownik)

# ulamek1 = Ulamek(1, 2)
# ulamek2 = Ulamek(5, 2)
#
# pomnozony = ulamek1.mnoz(ulamek2)
# ulamek1.humanize()
