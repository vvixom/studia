from ulamek_2_1 import Ulamek

class NowyUlamek(Ulamek):

    def mnoz(self, ulamek):
        New_ulam = super().mnoz(ulamek)

        licz = New_ulam.licznik
        mian = New_ulam.mianownik

        nwd = self.nwd(licz, mian)

        return Ulamek(licz/nwd, mian/nwd)

    def nwd(self, m, n):
        while True:
            r = m % n
            if not r:
                return n
            m, n = n, r

ulamek1 = NowyUlamek(2, 8)
ulamek2 = NowyUlamek(2, 4)

pomnozony = ulamek1.mnoz(ulamek2)

print(pomnozony.licznik)
print(pomnozony.mianownik)
